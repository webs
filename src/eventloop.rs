/* This is a thin wrapper around mio for building state machines on a shared event loop.
 * It's similar in spirit to Rotor, but with a bunch of simplifications and other differences.
 * (Rotor is currently in too much flux and its ecosystem very incomplete/alpha)
 *
 * = Machine -> Eventloop interaction
 *
 * The biggest difference (I think) between Rotor and this is the way it deals with the problem of
 * communicating actions back from the Machine to the EventLoop. Since the EventLoop is the owner
 * of the Machine, it is not possible to give a mutable borrow of the EventLoop to a method in the
 * Machine, as that what will result in a double mutable borrow. This problem is inherent in the
 * architecture and Rust is correct to disallow the borrow; since it would be possible to mutate
 * the Machine object through multiple aliases.
 *
 * Rotor solves this problem by having the method handlers return a "response", i.e. an action that
 * it wants the EventLoop to perform. This implementation is more flexible but slightly more hacky:
 * When calling a method in the Machine, the Machine object itself is temporarily removed from the
 * EventLoop object, thus removing the alias and allowing both objects to be Mutably borrowed.
 * Downside: This solution is more prone to logic errors that Rust can't catch, like removing or
 * re-using the MToken while inside a Machine handler.
 *
 * = Some rants on mio::timer
 *
 * This implementation wraps mio::timer in a way to provide an API that is similar to IO
 * registration. The mio-provided API has a few odd warts that make it hard to use:
 * - On registration you get a Timeout object, but on poll you get your custom token
 * - On poll you only get your custom token, not the previous Timeout object. In fact, the Timeout
 *   object has implicitly become invalid.
 * - The Timeout object is needed in order to change the timeout or cancel it
 *
 * This wrapping provides extra overhead because we need to keep track of each timer registration.
 * Some other weak points of mio::timer:
 * - It spawns a thread in order to signal timeouts. To me it seems much easier and efficient to
 *   simply provide a timeout value that can be passed to Poll::poll().
 * - It does an up-front allocation for all timer objects, which is a waste of memory if the upper
 *   bound on the number of timers is much higher than what is actually reached (likely), and
 *   forces me to calculate this upper bound in the first place. (It also requires me to add
 *   ugly-looking unwrap()'s to function calls that should never fail)
 *
 *
 * TODO: Machines don't care on which thread they run, so as a scalability improvement it's
 * possible to spawn a configurable number of threads on start-up, run a separate event loop on
 * each, and assign each Machine to a different event loop.
 */
use mio;
use mio::{Poll,Events,Token};
use mio::timer::{Timer,Timeout};
use std;
use std::time::Duration;
use slab::Slab;


// TODO: Upstream this to Slab crate?
fn slab_insert<T, I:Into<usize>+From<usize>>(s: &mut Slab<T,I>, t: T) -> I {
    s.insert(t).or_else(|t| {
        // Grow by some small fixed number. From what I can see the Slab implementation already
        // does exponential growth internally.
        s.reserve_exact(8);
        s.insert(t)
    }).unwrap_or_else(|_| { unreachable!() })
}


// Reserved token for the timer. Assuming this value is never allocated inside a Slab.
// Note that mio already uses MAX internally, and this is undocumented.
// Such a fragile API. -.-
const TIMER_TOKEN: Token = Token(std::usize::MAX-1);


pub enum Action {
    Remove
}

pub trait Machine {
    fn handle(&mut self, &mut Context, mio::Event) -> Option<Action>;
    fn timeout(&mut self, &mut Context, TToken) -> Option<Action>;
    // Called right before the Machine is destroyed, allows it to deregister() and
    // cancel_timeout().
    // TODO: This cleanup is better done automatically, either at removal of the Machine or through
    // a guard object given to the Machine.
    fn remove(&mut self, &mut Context) { }
}


#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)] pub struct MToken(pub usize);
impl From<usize> for MToken { fn from(val: usize) -> MToken { MToken(val) } }
impl From<MToken> for usize { fn from(val: MToken) -> usize { val.0 } }

#[derive(Copy, Clone, Debug, PartialEq, Eq, PartialOrd, Ord, Hash)] pub struct TToken(pub usize);
impl From<usize> for TToken { fn from(val: usize) -> TToken { TToken(val) } }
impl From<TToken> for usize { fn from(val: TToken) -> usize { val.0 } }


pub struct EventLoop {
    poll: Poll,
    regs: Slab<Option<MToken>, Token>,
    timer: Timer<TToken>,
    timers: Slab<Option<(MToken,Timeout)>, TToken>,
    // A machine entry is set to None during a method call on the Machine object.
    machines: Slab<Option<Box<Machine>>, MToken>,
}

pub struct Context<'a> {
    parent: &'a mut EventLoop,
    machine: MToken,
}


impl<'a> Context<'a> {
    pub fn reg_alloc(&mut self) -> Token {
        slab_insert(&mut self.parent.regs, None)
    }

    pub fn reg_free<E: ?Sized>(&mut self, io: &E, token: Token) where E: mio::Evented {
        if let Some(Some(_)) = self.parent.regs.remove(token) {
            self.parent.poll.deregister(io).unwrap();
        }
    }

    pub fn reg_set<E: ?Sized>(&mut self, io: &E, token: Token, interest: mio::Ready) where E: mio::Evented {
        let r = self.parent.regs.entry(token).expect("Invalid token in reg_set").replace(Some(self.machine));
        match r {
            None    => { self.parent.poll.  register(io, token, interest, mio::PollOpt::level()).unwrap(); }
            Some(_) => { self.parent.poll.reregister(io, token, interest, mio::PollOpt::level()).unwrap(); },
        }
    }

    pub fn reg_unset<E: ?Sized>(&mut self, io: &E, token: Token) where E: mio::Evented {
        let mut r = self.parent.regs.get_mut(token).expect("Invalid token in reg_unset");
        if let Some(_) = *r {
            self.parent.poll.deregister(io).unwrap();
        }
        *r = None;
    }


    pub fn timeout_alloc(&mut self) -> TToken {
        slab_insert(&mut self.parent.timers, None)
    }

    pub fn timeout_free(&mut self, token: TToken) {
        if let Some(Some((_, ref timeout))) = self.parent.timers.remove(token) {
            self.parent.timer.cancel_timeout(timeout);
        }
    }

    pub fn timeout_set(&mut self, token: TToken, d: Duration) {
        let newval = Some((self.machine, self.parent.timer.set_timeout(d, token).unwrap()));
        let oldval = self.parent.timers.entry(token).expect("Invalid token in timeout_unset").replace(newval);
        if let Some((_, ref timeout)) = oldval {
            self.parent.timer.cancel_timeout(timeout);
        }
    }

    pub fn timeout_unset(&mut self, token: TToken) {
        let oldval = self.parent.timers.entry(token).expect("Invalid token in timeout_unset").replace(None);
        if let Some((_, ref timeout)) = oldval {
            self.parent.timer.cancel_timeout(timeout);
        }
    }


    pub fn spawn<F>(&mut self, f: F) where F: Send + 'static + FnOnce(&mut Context) -> Box<Machine> {
        self.parent.spawn(f);
    }
}


impl EventLoop {
    pub fn new() -> EventLoop {
        let timer = mio::timer::Builder::default()
            // Timers are used for (1) aborting a connection or process after some timeout and
            // (2) back-off after listen error. (1) will be fine with 1-second precision, and
            // (2) should be incredibly rare. The current value is terribly imprecise for a
            // generic timer, but should suffice for our limited use case.
            .tick_duration(Duration::from_millis(200))
            // TODO: Calculate a more exact upper limit. I'd prefer mio to just resize internal
            // structures on demand, but it can't do that (yet). The default value allocates
            // 2MB of memory, which is far too wasteful.
            .capacity(4_000).build();

        let poll = Poll::new().unwrap();
        poll.register(&timer, TIMER_TOKEN, mio::Ready::readable(), mio::PollOpt::edge()).unwrap();

        EventLoop {
            poll: poll,
            regs: Slab::with_capacity(16),
            timer: timer,
            timers: Slab::with_capacity(16),
            machines: Slab::with_capacity(16),
        }
    }

    pub fn spawn<F>(&mut self, f: F) where F: Send + 'static + FnOnce(&mut Context) -> Box<Machine> {
        let mtoken = slab_insert(&mut self.machines, None);
        trace!("[{}] Spawning machine", mtoken.0);
        let machine = {
            let mut ctx = Context{ parent: self, machine: mtoken };
            f(&mut ctx)
        };
        self.machines[mtoken] = Some(machine);
    }

    // XXX: I don't get why "&mut Machine" doesn't work in the FnOnce.
    fn dispatch<F>(&mut self, mtoken: MToken, f: F)
            where F: FnOnce(&mut Box<Machine>, &mut Context) -> Option<Action> {
        let mut machine = match self.machines.entry(mtoken) {
            None => { return; },
            Some(mut x) => { x.replace(None).unwrap() },
        };

        let action = {
            let mut ctx = Context{ parent: self, machine: mtoken };
            f(&mut machine, &mut ctx)
        };

        if action.is_none() {
            self.machines[mtoken] = Some(machine);
        } else {
            trace!("[{}] Removing machine", mtoken.0);
            let mut ctx = Context{ parent: self, machine: mtoken };
            machine.remove(&mut ctx);
        }
    }

    fn dispatch_io(&mut self, event: mio::Event) {
        if let Some(&Some(mtoken)) = self.regs.get(event.token()) {
            self.dispatch(mtoken, |m, ctx| {
                trace!("[{}] Calling handle for event {} state {:?}", mtoken.0, event.token().0, event.kind());
                m.handle(ctx, event)
            });
        }
    }

    fn dispatch_timeout(&mut self) {
        while let Some(ttoken) = self.timer.poll() {
            let mtoken = self.timers.entry(ttoken).and_then(|mut e| { e.replace(None).map(|(t,_)| { t }) });
            if let Some(mtoken) = mtoken {
                self.dispatch(mtoken, |m, ctx| {
                    trace!("[{}] Calling timeout", mtoken.0);
                    m.timeout(ctx, ttoken)
                });
            }
        }
    }

    pub fn run(&mut self) {
        let mut events = Events::with_capacity(64);
        debug!("Entering event loop");
        loop {
            if let Ok(_) = self.poll.poll(&mut events, None) {
                for event in events.iter() {
                    if event.token() == TIMER_TOKEN {
                        self.dispatch_timeout();
                    } else {
                        self.dispatch_io(event);
                    }
                }
            }
        }
    }
}
