/* I'm playing around. Let me.
#![allow(dead_code)]
#![allow(unused_variables)]
#![allow(unused_mut)]
#![allow(unused_imports)]
// */
#[macro_use] extern crate nom;
#[macro_use] extern crate log;
extern crate env_logger;
extern crate getopts;
extern crate mio;
extern crate slab;
extern crate netbuf;
extern crate httparse;

mod iostream;
mod config;
mod eventloop;
mod listener;
mod itf_http;
mod request;

use std::process::exit;
use std::io::prelude::*;


struct CliOpts {
    config: String,
}

impl CliOpts {
    fn new() -> CliOpts {
        CliOpts {
            config: "/etc/webs.conf".to_string()
        }
    }

    fn parse(mut self) -> CliOpts {
        let mut args = std::env::args();
        let prog = args.next().unwrap();

        let mut opts = getopts::Options::new();
        opts.optflag("h", "help", "print this help menu");
        opts.optflag("V", "version", "show program version");
        opts.optopt("c", "config", "config file", "FILE");

        let m = match opts.parse(args) {
            Ok(m) => { m }
            Err(e) => {
                let _ = writeln!(std::io::stderr(), "{}", e);
                exit(1);
            }
        };
        if m.opt_present("h") {
            let _ = println!("{}", opts.usage(&prog));
            exit(0);
        }
        if m.opt_present("V") {
            let _ = println!("{} {}", prog, option_env!("CARGO_PKG_VERSION").unwrap_or(""));
            exit(0);
        }
        if let Some(f) = m.opt_str("c") {
            self.config = f;
        }

        self
    }
}


fn main() {
    let opts = CliOpts::new().parse();
    let conf = config::Config::parse(&opts.config).unwrap();
    env_logger::init().unwrap();
    trace!("Configuration read from {}: {:?}", opts.config, conf);

    let mut main = eventloop::EventLoop::new();
    listener::setup(&mut main, &conf).unwrap();
    main.run();
}
