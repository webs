use mio::Event;
use mio::tcp::TcpStream;
use std::net::SocketAddr;
use std::str::FromStr;
use std::sync::Arc;
use httparse;

use request;
use iostream::IoStream;
use listener::Interface;
use eventloop::{Machine,Context,TToken,Action};


const MAX_HEADERS: usize = 128;

// This isn't really a hard maximum on the length of the headers, it's more a "stop reading stuff
// from the network if we've at least read this many bytes and still haven't received a complete
// header" limit. The header can exceed this limit if the last network read got us more bytes than
// MAX_HEADER_LEN.
const MAX_HEADER_LEN: usize = 4096;


pub struct ItfHttp {
    io: IoStream,
    addr: SocketAddr,
    itf: Arc<Interface>,
}


macro_rules! try_rm {
    ( $s: expr, $e: expr ) => {{
        match $e {
            Err(e) => {
                debug!("{}: {}", $s.addr, e);
                return Some(Action::Remove);
            },
            Ok(v) => v
        }
    }};
}


impl ItfHttp {
    pub fn new(ctx: &mut Context, itf: Arc<Interface>, sock: TcpStream, addr: SocketAddr) -> ItfHttp {
        let io = IoStream::new(ctx, sock, itf.cfg.io_timeout, itf.cfg.io_timeout, 4096);
        ItfHttp {
            io: io,
            addr: addr,
            itf: itf,
        }
    }

    // TODO: This error value is both ugly, inefficient and doesn't provide the info necessary to
    // determine what kind of response we're supposed to give on error.
    fn get_req(&mut self) -> Result<Option<request::Request>, String> {
        // I'm not totally comfortable with having a fixed maximum on the number of headers and
        // with allocating 4 KiB on the stack. But alas, it's what everyone else does.
        let mut headers = [httparse::EMPTY_HEADER; MAX_HEADERS];

        // Some weird indirection here to handle lifetime entanglement with the httparse object,
        // input buffer and the headers list.
        {
            let mut parser = httparse::Request::new(&mut headers);
            let res = try!(parser.parse(&self.io.rbuf[..]).map_err(|e| format!("{}", e)));
            if let httparse::Status::Complete(size) = res {
                Ok(Some((size, request::Request::new(
                    self.addr.ip(),
                    try!(request::Method::from_str(parser.method.unwrap()).map_err(|e| format!("{}", e))),
                    parser.path.unwrap(),
                    parser.version.unwrap()
                ))))
            } else {
                if self.io.rbuf.len() > MAX_HEADER_LEN {
                    return Err("Headers too long".to_string())
                }
                Ok(None)
            }
        }.map(|x| x.map(|(size, mut r)| {
            for h in &headers[..] {
                if *h == httparse::EMPTY_HEADER {
                    break;
                }
                r.headers.data.push(request::Header::new(h.name, h.value));
            }
            r.headers.data.sort();
            self.io.rbuf.consume(size);
            r
        }))
    }
}


impl Machine for ItfHttp {
    fn handle(&mut self, ctx: &mut Context, ev: Event) -> Option<Action> {
        try_rm!(self, self.io.handle(ctx, ev));

        if let Some(req) = try_rm!(self, self.get_req()) {
            debug!("{} {} {}", self.addr, req.method, req.path);
        }
        self.io.set_ioreg(ctx);

        None
    }

    fn timeout(&mut self, _: &mut Context, t: TToken) -> Option<Action> {
        try_rm!(self, self.io.timeout(t));
        None
    }

    fn remove(&mut self, ctx: &mut Context) {
        self.itf.release();
        self.io.remove(ctx);
    }
}
